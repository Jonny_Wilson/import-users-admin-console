# TO-DO

## import-users-admin-console
Programa para geração de arquivos de importação do admin console para alunos e professores. 

### Detalhes da API RESTful
A API RESTful <Mnome da API> contém as seguintes características:  
* Projeto criado com Spring Boot x.x.x e Java x.x.x
* Banco de dados MySQL com JPA e Spring Data JPA
* Autenticação e autorização com Spring Security e tokens JWT (JSON Web Token)
* Migração de banco de dados com Flyway
* Testes unitários e de integração com JUnit e Mockito
* Caching com EhCache
* Integração contínua com TravisCI

### Como executar a aplic
