package br.com.pmjp.importadmin.io;

import java.util.ArrayList;
import java.util.List;

import br.com.pmjp.importadmin.model.Pessoa;
import br.com.pmjp.importadmin.model.TipoUsuarioEnum;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ArquivoInput {

	private TipoUsuarioEnum tipoUsuarioEnum;
	private List<Pessoa> pessoas;
	
	public ArquivoInput() {
		this.pessoas = new ArrayList<Pessoa>();
	}
}
