package br.com.pmjp.importadmin.io;

import java.util.List;

import br.com.pmjp.importadmin.model.TipoUsuarioEnum;
import br.com.pmjp.importadmin.model.Usuario;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ArquivoOutput {
	
	private TipoUsuarioEnum tipoUsuarioEnum;
	List<Usuario> usuarios;

}
