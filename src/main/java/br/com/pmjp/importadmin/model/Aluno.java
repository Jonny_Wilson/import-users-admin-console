package br.com.pmjp.importadmin.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class Aluno {
	// campos da planilha: https://docs.google.com/spreadsheets/d/1HYb2OTlFUdIa4c8250CltYCcU_eYSIkhhec6se_Fcr4/edit#gid=0 //CRIAR ALUNO

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int Id;
	
	private String status;
	private String mensagemErro;
	private String nome;
	private String email;
	private String unidadeOrganizacional;
	private String matricula;
	private String cpf;
	private String dataNascimento;
	private String nomeResponsavel;
	private String ensino;
	private String inepEscola;
	private String nomeEscola;
	private String serie;
	private String turma;
	private String nomeTurma;
	
	
}
