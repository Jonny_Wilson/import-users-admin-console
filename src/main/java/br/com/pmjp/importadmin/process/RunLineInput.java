package br.com.pmjp.importadmin.process;

import java.util.regex.Pattern;

import br.com.pmjp.importadmin.model.Pessoa;


public class RunLineInput {

	public static Pessoa processLine(String line){
		String [] lineSplited = line.split(Pattern.quote(","));
		Pessoa pessoa = new Pessoa(null, lineSplited[0], lineSplited[1], lineSplited[2], lineSplited[3], lineSplited[4], lineSplited[5]);
		return pessoa;
	}
}
