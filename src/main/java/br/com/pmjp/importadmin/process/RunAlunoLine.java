package br.com.pmjp.importadmin.process;

import java.io.FileWriter;
import java.io.IOException;

import br.com.pmjp.importadmin.model.Usuario;


public class RunAlunoLine {
	public static void processLine(FileWriter arquivoWriter, Usuario usuario) throws IOException{
		// campos da planilha: https://docs.google.com/spreadsheets/d/1HYb2OTlFUdIa4c8250CltYCcU_eYSIkhhec6se_Fcr4/edit#gid=0 //CRIAR ALUNO
//		firstName; // required
		arquivoWriter.write(usuario.getFirstName());
		arquivoWriter.write(",");
		
//		lastName; // required
		arquivoWriter.write(usuario.getLastName());
		arquivoWriter.write(",");
		
//		emailAddress; // required
		arquivoWriter.write(usuario.getEmailAddress());
		arquivoWriter.write(",");
		
//		password; // required
		arquivoWriter.write(usuario.getPassword());
		arquivoWriter.write(",");
		
//		passwordHashFunction; // upload only
		arquivoWriter.write("");
		arquivoWriter.write(",");
		
//		orgUnitPath; // required
		arquivoWriter.write(usuario.getOrgUnitPath());
		arquivoWriter.write(",");
		
//		newPrimaryEmail; // upload only
		arquivoWriter.write("");
		arquivoWriter.write(",");
		
//		recoveryEmail;
		arquivoWriter.write("");
		arquivoWriter.write(",");
		
//		homeSecondaryEmail;
		arquivoWriter.write("");
		arquivoWriter.write(",");
		
//		workSecondaryEmail;
		arquivoWriter.write("");
		arquivoWriter.write(",");
		
//		recoveryPhone; // [MUST BE I N THE E.164 FORMAT]
		arquivoWriter.write("");
		arquivoWriter.write(",");
		
//		workPhone;
		arquivoWriter.write("");
		arquivoWriter.write(",");
		
//		homePhone;
		arquivoWriter.write("");
		arquivoWriter.write(",");
		
//		mobilePhone;
		arquivoWriter.write("");
		arquivoWriter.write(",");
		
//		workAddress;
		arquivoWriter.write("");
		arquivoWriter.write(",");
		
//		homeAddress;
		arquivoWriter.write("");
		arquivoWriter.write(",");
		
//		employeeID;
		arquivoWriter.write("");
		arquivoWriter.write(",");
		
//		employeeType;
		arquivoWriter.write("");
		arquivoWriter.write(",");
		
//		employeeTitle;
		arquivoWriter.write("");
		arquivoWriter.write(",");
		
//		managerEmail;
		arquivoWriter.write("");
		arquivoWriter.write(",");
		
//		department;
		arquivoWriter.write("");
		arquivoWriter.write(",");
		
//		costCenter;
		arquivoWriter.write("");
		arquivoWriter.write(",");
		
//		buildingID;
		arquivoWriter.write("");
		arquivoWriter.write(",");
		
//		floorName;
		arquivoWriter.write("");
		arquivoWriter.write(",");
		
//		floorSection;
		arquivoWriter.write("");
		arquivoWriter.write(",");
		
//		changePasswordatNextSignIn;
		arquivoWriter.write("");
		arquivoWriter.write(",");
		
//		newStatus; // upload only
		arquivoWriter.write("");
		arquivoWriter.write(",");
		
//		newLicenses; // upload only
		arquivoWriter.write("");
		arquivoWriter.write(",");
		
//		advancedProtectionProgramEnrollment;
		arquivoWriter.write("");
		arquivoWriter.write(System.lineSeparator());
	}
}
