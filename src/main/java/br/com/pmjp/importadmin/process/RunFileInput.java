package br.com.pmjp.importadmin.process;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import br.com.pmjp.importadmin.io.ArquivoInput;
import br.com.pmjp.importadmin.model.Pessoa;
import br.com.pmjp.importadmin.model.Usuario;
import br.com.pmjp.importadmin.utils.PessoaUtils;
import lombok.Getter;

public class RunFileInput {
	
	@Getter
	private File pessoaFile;
	
	@Getter
	private File usuarioGoogleFile;

	public static void runImport(String arquivoPessoa) throws IOException {
		// open arquivo de input
		Scanner scanner = new Scanner(new File(arquivoPessoa));
		
		// open arquivo de output
		// TODO trocar o nome do arquivo
		FileWriter writer = new FileWriter("arquivo_saida.csv"); 
		
		// ler linha de arquivo
		ArquivoInput arquivoInput = new ArquivoInput();
		while(scanner.hasNext()) {
			Pessoa pessoa = RunLineInput.processLine(scanner.next());
			arquivoInput.getPessoas().add(pessoa);
		}
		
		
		
		
		// converter pessoa para usuario
		// copiar linha usu�rio no arquivo de output
		for(Pessoa pessoa: arquivoInput.getPessoas()) {
			Usuario usuario = PessoaUtils.convertToUsuario(pessoa);
			
			RunAlunoLine.processLine(writer, usuario);

		}
		
	}
}
