package br.com.pmjp.importadmin.process;

import java.io.IOException;

public class ApplicationRun {

	public static void main(String[] args) {
		validarArguments(args);
		
		System.out.println("---------- RUN IMPORT ----------");
		
		try {
			RunFileInput.runImport(args[0]);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void validarArguments(String[] args) {
		if (args.length < 1) {
			System.out.println("Help: terminal/cmd : importadmin.jar <arquivo-input>");
			System.exit(0);
		} else if (args.length > 1) {
			System.out.println("Help: terminal/cmd : importadmin.jar <arquivo-input>");
			System.exit(0);
		}
	}

}
