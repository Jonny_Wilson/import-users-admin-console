package br.com.pmjp.importadmin.utils;

import java.util.regex.Pattern;

import br.com.pmjp.importadmin.model.Pessoa;
import br.com.pmjp.importadmin.model.Usuario;


public class PessoaUtils {
	public static Usuario convertToUsuario(Pessoa pessoa) {
		// get first name
		String firstName = getFirstName(pessoa.getNome());
		// get last name
		String lastName = getLastName(pessoa.getNome());
		// generate email
		String email = generateEmail(pessoa);
		// generate password
		String password = generatePassword(pessoa);
		// get org unit
		String orgUnit = getOrgUnit(pessoa);
		
		return new Usuario(firstName, lastName, email, password, orgUnit);
	}

	private static String getFirstName(String nome) {
		String firstName = splitName(Pattern.quote(" "))[0];
		return firstName;
	}

	private static String getLastName(String nome) {
		String[] splitedName = splitName(Pattern.quote(" "));
		return splitedName[splitedName.length-1];
	}

	private static String generateEmail(Pessoa pessoa) {
		
		// primeiroNome.
		StringBuilder email = new StringBuilder();
		email.append(getFirstName(pessoa.getNome()));
		switch (pessoa.getTipoUsuario()) {
		case ALUNO:
			
			break;
		case PROFESSOR:
			
			break;

		default:
			break;
		}
		email.append(".joaopessoa.pb.gov.br");
		return null;
	}

	private static String generatePassword(Pessoa pessoa) {
		return getLastName(pessoa.getNome());
	}

	private static String getOrgUnit(Pessoa pessoa) {
		return "unidade organizacional";
	}
	
	private static String[] splitName(String nome) {
		String[] splitedName = nome.split(Pattern.quote(" "));
		return splitedName;
	}
}
