package br.com.pmjp.importadmin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ImportadminApplication {

	public static void main(String[] args) {
		SpringApplication.run(ImportadminApplication.class, args);
	}

}
